const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string") {
    return false;
  }

  if (isNaN(+str1) || isNaN(+str2)) {
    return false;
  }

  return String(+str1 + +str2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let postCount = listOfPosts.reduce((result, current) => {
    if (current.author === authorName) {
      return ++result;
    }
    return result;
  }, 0);

  let commentCount = listOfPosts.reduce((result, current) => {
    if ("comments" in current) {
      for (let comment of current.comments) {
        if (comment.author === authorName) {
          ++result;
        }
      }
      return result;
    }
    return result;
  }, 0);

  return `Post:${postCount},comments:${commentCount}`;
};

const tickets = (people) => {
  let currentBalance = 0;

  for (let payment of people) {
    if (+payment == 25) {
      currentBalance += +payment;
      continue;
    }

    if (+payment - 25 <= currentBalance) {
      currentBalance += +payment - 25;
    } else {
      return "NO";
    }
  }

  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
